<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'coming-soon', 'namespace' => 'Api', 'middleware' => 'coming.soon'], function() {
    Route::any('test', 'TestController@test');
    Route::any('ping', 'ConfigController@ping');

    Route::any('get-app-data', 'GetApplicationConfigController');
    Route::post('subscribe-news-letter', 'NewsLetterController@subscribe');
    Route::post('onboard-client', 'OnboardClientController@getStarted');
    Route::post('onboard-merchant', 'OnboardMerchantController@getStarted');
    Route::post('contact-us', 'ContactUsController@contactUs');
});
