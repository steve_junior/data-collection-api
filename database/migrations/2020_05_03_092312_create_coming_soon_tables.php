<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComingSoonTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacted_us', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email_or_phone')->nullable();
            $table->text('narration')->nullable();

            $table->unsignedBigInteger('request_log_id')->index();
            $table->timestamps();
        });

        Schema::create('prospect_customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('company_name')->nullable();
            $table->string('income_range')->nullable();
            $table->string('income_stream')->nullable();
            $table->text('narration')->nullable();
            $table->timestamps();


            $table->unsignedBigInteger('request_log_id')->index();
        });

        Schema::create('prospect_merchants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('company_name')->nullable();
            $table->string('location_or_website')->nullable();
            $table->string('category')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('narration')->nullable();
            $table->timestamps();

            $table->unsignedBigInteger('request_log_id')->index();
        });

        Schema::create('news_letter_subscribers', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('email')->nullable();

            $table->unsignedBigInteger('request_log_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $coming_soon_tables = ['contacted_us', 'prospect_customers', 'prospect_merchants', 'news_letter_subscribers'];

        foreach ($coming_soon_tables as $table) {
            Schema::dropIfExists($table);
        }
    }
}
