<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 03/05/2020
 * Time: 10:07 AM
 */

namespace App\Misc;

use App\Models\RequestLog;
use Illuminate\Http\Request;

abstract class Helper {
    public static function response_structure($data = [], $message  = 'Something Went Wrong', $success = false) {
        return ['data' => $data, 'message' => $message, 'success' => $success];
    }

    public static function get_request_log_id(Request $request) {

        $payload = [
            'endpoint' => $request->decodedPath(),
            'type'     => $request->getMethod(),
            'source'   => $request->getClientIp(),
            'payload'  => json_encode($request->input()),
            'user_agent' => $request->userAgent()
        ];

        $log = RequestLog::create($payload);

        return $log->id;
    }

    public static function get_income_range($key = null){
        $ranges = [
            'below_500'         => 'Below GHS 500',
            'between_500_1500'  => 'GHS 500 - 1500',
            'between_1500_3000' => 'GHS 1500 - 3000',
            'between_3000_5000' => 'GHS 3000 - 5000',
            'above_5000'        => 'GHS 5000+',
        ];

        if(isset($key)){
            return $ranges[$key];
        }

        return $ranges;
    }

    public static function get_income_stream($key = null){
        $streams = [
            'salary' => 'Earned Income (Salary)',
            'profit' => 'Business Venture (Profit)',
            'investment' => 'Interest/Dividend (Investment)',
            'rental-services' => 'Rental Income (Lending)',
            'student' => 'Undergraduate'
        ];

        if(isset($key)){
            return $streams[$key];
        }

        return $streams;
    }
}

