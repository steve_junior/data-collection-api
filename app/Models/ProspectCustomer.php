<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 03/05/2020
 * Time: 9:19 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProspectCustomer extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'company_name', 'income_range', 'income_stream', 'narration', 'request_log_id'];

    protected $hidden = ['id', 'request_log_id', 'updated_at'];
}
