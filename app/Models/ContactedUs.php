<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 03/05/2020
 * Time: 9:20 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ContactedUs extends Model
{
    protected $fillable = ['email_or_phone', 'narration', 'request_log_id'];

    protected $hidden = ['id', 'request_log_id', 'updated_at'];
}
