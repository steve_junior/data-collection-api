<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 03/05/2020
 * Time: 10:12 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class RequestLog extends Model
{
    protected $fillable  = ['endpoint', 'type', 'source', 'payload', 'user_agent'];
}
