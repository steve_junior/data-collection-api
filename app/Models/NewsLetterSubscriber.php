<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 03/05/2020
 * Time: 12:29 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class NewsLetterSubscriber extends Model
{
    protected $table = 'news_letter_subscribers';

    protected $fillable = ['email', 'request_log_id'];

    protected $hidden = ['id', 'request_log_id', 'updated_at'];
}
