<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 03/05/2020
 * Time: 9:19 AM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ProspectMerchant extends Model
{
    protected $fillable = ['company_name', 'location_or_website', 'category', 'email', 'phone', 'narration', 'request_log_id'];

    protected $hidden = ['id', 'request_log_id', 'updated_at'];
}
