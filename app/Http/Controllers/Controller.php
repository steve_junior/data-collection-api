<?php

namespace App\Http\Controllers;

use App\Misc\Helper;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $request_log_id;

    public function __construct(Request $request)
    {
        $this->request_log_id = Helper::get_request_log_id($request);
    }


}
