<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Misc\Helper;
use App\Models\NewsLetterSubscriber;
use Illuminate\Http\Request;

class NewsLetterController extends Controller
{
    public function subscribe(Request $request){
        $payload = [
            'email'          => $request->email,
            'request_log_id' => $this->request_log_id
        ];

        $model = NewsLetterSubscriber::create($payload);

        $response = Helper::response_structure();

        if($model){
            $response['message'] = 'Successful';
            $response['success'] = true;
            $response['data']    = $model;
        }

        return response()->json($response);
    }
}
