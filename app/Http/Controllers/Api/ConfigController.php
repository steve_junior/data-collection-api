<?php
/**
 * Created by PhpStorm.
 * User: stevejunior
 * Date: 18/07/2020
 * Time: 11:47 AM
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ConfigController extends Controller
{
    public function ping(Request $request){
        return 'pong';
    }
}
