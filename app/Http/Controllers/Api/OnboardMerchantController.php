<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Misc\Helper;
use App\Models\ProspectMerchant;
use Illuminate\Http\Request;

class OnboardMerchantController extends Controller
{
    public function getStarted(Request $request)
    {
        $payload = [
            'email'          => $request->get('email'),
            'phone'          => $request->get('phone'),
            'company_name'   => $request->get('company_name'),
            'category'       => $request->get('category'),
            'narration'      => $request->get('narration'),
            'location_or_website' => $request->get('location_or_website'),
            'request_log_id' => $this->request_log_id
        ];

        $model = ProspectMerchant::create($payload);

        $response = Helper::response_structure();

        if($model){
            $response['message'] = 'Successful';
            $response['success'] = true;
            $response['data'] = $model;
        }

        return response()->json($response, 200, ['Accept' => 'application/json']);
    }
}
