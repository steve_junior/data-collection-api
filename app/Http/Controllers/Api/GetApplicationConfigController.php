<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Misc\Helper;
use Illuminate\Http\Request;

class GetApplicationConfigController extends Controller
{
    public function __invoke(Request $request)
    {
        $response = Helper::response_structure();

        $inc_streams = Helper::get_income_stream();
        $inc_ranges  = Helper::get_income_range();

        $response['data']['config']['income'] = ['streams' => $inc_streams, 'ranges' => $inc_ranges];
        $response['message'] = 'Successful';
        $response['success'] = true;

        return response()->json($response);
    }
}
