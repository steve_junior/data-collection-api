<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Misc\Helper;
use App\Models\ContactedUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function contactUs(Request $request){
        $payload = [
            'email_or_phone' => $request->email_or_phone,
            'narration'       => $request->narration,
            'request_log_id' => $this->request_log_id
        ];

        $model = ContactedUs::create($payload);

        $response = Helper::response_structure();

        if($model){
            $response['message'] = 'Successful';
            $response['success'] = true;
            $response['data']    = $model;
        }

        return response()->json($response);
    }
}
