<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Misc\Helper;
use App\Models\ProspectCustomer;
use Illuminate\Http\Request;


class OnboardClientController extends Controller
{

    public function getStarted(Request $request)
    {
        $payload = [
            'name'           => $request->name,
            'email'          => $request->email,
            'phone'          => $request->phone,
            'company_name'   => $request->company_name,
            'income_range'   => Helper::get_income_range($request->income_range),
            'income_stream'  => Helper::get_income_stream($request->income_stream),
            'narration'      => $request->narration,
            'request_log_id' => $this->request_log_id
        ];

        $model = ProspectCustomer::create($payload);

        $response = Helper::response_structure();

        if($model){
            $response['message'] = 'Successful';
            $response['success'] = true;
            $response['data'] = $model;
        }

        return response()->json($response, 200, ['Accept' => 'application/json']);
    }
}
