<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Misc\Helper;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function test(Request $request){

        $response = Helper::response_structure(null, 'Test was successful', true);

        return response()->json($response);
    }

    public function ping(Request $request){

        return 'pong';
    }
}
